package com.tickets;

import java.util.Scanner;

/**
 * Klasa TicketPriceProvider przechowuje obiekty biletow i zarzadza nimi.
 * @see NormalTicket
 * @see ReducedTicket
 */
public class TicketPriceProvider {
    private final NormalTicket normalTicket = new NormalTicket();
    private final ReducedTicket reducedTicket = new ReducedTicket();

    /**
     * Metoda pobierajaca od uzytkownika ceny biletow i wywolująca metode do ich ustawienia.
     */
    public void settingPrices(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter normal ticket price [10-20] (it will affect its popularity!): ");
        int value;
        do{
            while(!scanner.hasNextInt()){
                String error = scanner.next();
                System.out.println(error + " is not a number! Try again:");
            }
            value = scanner.nextInt();
            if(value<10 || value>20){
                System.out.println("Not allowed number! Try again:");
            }
        }while(value<10 || value>20);

        normalTicket.setPriceAndPopularity(value);
        reducedTicket.setPrice(value);
    }

    /**
     * Metoda do pobrania obiektu biletu normalnego.
     * @return obiekt normalnego biletu
     */
    public NormalTicket getNormalTicket() {
        return normalTicket;
    }

    /**
     * Metoda do pobrania obiektu biletu ulgowego.
     * @return obiekt ulgowego biletu
     */
    public ReducedTicket getReducedTicket() {
        return reducedTicket;
    }
}
