package com.tickets;

/**
 * Klasa ReducedTicket przechowuje cene biletu ulgowego.
 */
public class ReducedTicket {
    private double price;

    /**
     * Metoda do ustalenia ceny biletu ulgowego.
     * @param price cena wprowadzona przez uzytkownika
     */
    public void setPrice(int price) {
        this.price = price * 0.75;
    }

    /**
     * Metoda do pobrania ceny biletu ulgowego.
     * @return cena biletu ulgowego
     */
    public double getPrice() {
        return price;
    }
}
