package com.tickets;

/**
 * Klasa NormalTicket przechowuje cene biletu normalnego oraz modyfikator popularnosci zalezny od ceny biletow.
 */
public class NormalTicket {
    private double price;
    private double popularity;

    /**
     * Metoda do ustalenia ceny biletu normalnego i modyfikatora popularnosci zaleznego od ceny biletow.
     * @param price cena wprowadzona przez użytkownika
     */
    public void setPriceAndPopularity(double price) {
        this.price = price;
        this.popularity = Math.round(((-price/25)+1.6)*100.0)/100.0;
    }

    /**
     * Metoda do pobrania ceny biletu normalnego.
     * @return cena biletu normalnego
     */
    public double getPrice() {
        return price;
    }

    /**
     * Metoda do pobrania modyfikatora popularnosci zaleznego od cen biletow.
     * @return modyfikator popularnosci zalezny od ceny biletow
     */
    public double getPopularity() {
        return popularity;
    }
}
