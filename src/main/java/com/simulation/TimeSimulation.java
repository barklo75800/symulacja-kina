package com.simulation;

import com.ReportFile;
import com.randomEvents.EventOccurrence;
import com.shop.SnackType;
import com.tickets.NormalTicket;
import com.tickets.ReducedTicket;

/**
 * Klasa TimeSimulation.
 * Zarządza czasem.
 * Przechowuje potrzebne informacje, przede wszystkim atkualny stan zadluzenia.
 * Wywoluje odpowiednie metody potrzebne do odpowiedniego przebiegu symulacji.
 * @see NormalTicket
 * @see ReducedTicket
 * @see SnackType
 * @see ReportFile
 */
public class TimeSimulation {
    private int dayNumber = 1;
    private int weekNumber = 0;
    private final int rooms, seats;
    private long debt;
    private final double equipmentPopularityModifier;
    private final NormalTicket normalTicket;
    private final ReducedTicket reducedTicket;
    private final SnackType popcorn;
    private final SnackType nachos;
    private final SnackType cola;
    private final boolean isRandomEventsEnabled;
    private final ReportFile report = new ReportFile();

    /**
     * Konstruktor klasy TimeSimulation.
     * @param rooms liczba sal w kinie
     * @param seats liczba miejsc w jednej sali
     * @param debt zadluzenie poczatkowe (inaczej calkowity koszt otwarcia kina)
     * @param equipmentPopularityModifier modyfikator popularnosci zalezny od rodzaju wyposazenia
     * @param normalTicket obiekt biletu normalnego
     * @param reducedTicket obiekt biletu ulgowego
     * @param popcorn obiekt przekaski popcorn
     * @param nachos obiekt przekaski nachos
     * @param cola obiekt przekaski cola
     * @param isRandomEventsEnabled boolean, stwierdzajacy czy zostaly wlaczone wydarzenia losowe
     */
    public TimeSimulation(int rooms, int seats, long debt, double equipmentPopularityModifier, NormalTicket normalTicket, ReducedTicket reducedTicket, SnackType popcorn, SnackType nachos, SnackType cola, boolean isRandomEventsEnabled){
        this.rooms = rooms;
        this.seats = seats;
        this.debt = debt;
        this.equipmentPopularityModifier = equipmentPopularityModifier;
        this.normalTicket = normalTicket;
        this.reducedTicket = reducedTicket;
        this.popcorn = popcorn;
        this.nachos = nachos;
        this.cola = cola;
        this.isRandomEventsEnabled = isRandomEventsEnabled;
        this.report.createReport();
    }

    /**
     * Metoda sprawdzajaca czy zostaly wlaczone wydarzenia losowe i wywolująca odpowiednie metody.
     * Jezeli zostaly wlaczone, to tworzy generator wydarzen losowych, w ktorym wywolywane sa symulacje dni.
     * Jezeli nie zostaly wlaczone, to wywoluje metode przeprowadzenia symulacji jednego dnia.
     * @see EventOccurrence
     */
    public void simulate(){
        if(isRandomEventsEnabled) {
            EventOccurrence eventOccurrence = new EventOccurrence();
            eventOccurrence.eventGenerator(this);
        }
        else{
            simulateOneDay();
        }
    }

    /**
     * Metoda odpowiedzialna za wywolanie odpowiedniej ilosci seansow oraz dodanie zarobkow z tych seansow do dlugu.
     * Odpowiednio zmienia wartosci nr dnia i nr tygodnia w celu prawidlowego liczenia czasu.
     * Po ostatnim dniu tygodnia wyswietla aktualny stan dlugu oraz wlicza pensje pracownikow i rachunki.
     * @see MovieIncome
     * @see Salary
     * @see Bills
     */
    public void simulateOneDay(){
        MovieIncome movieIncome = new MovieIncome(seats);
        for(int i = 0; i<rooms*4;i++){
            long income;
            income = movieIncome.simulateOneMovie(equipmentPopularityModifier, normalTicket, reducedTicket, popcorn, nachos, cola);
            addDebt(income);
            report.addTempIncome(income);
        }
        Salary salary = new Salary(rooms);
        Bills bills = new Bills(rooms);
        if(dayNumber==7){
            addDebt(salary.payDay());
            addDebt(bills.payments());
            dayNumber=1;
            weekNumber++;
            report.saveToFile(weekNumber);
            System.out.println("Debt after " + weekNumber + " week is: " + debt);
        }
        else{
            dayNumber++;
        }
    }

    /**
     * Metoda nie uwzglednia wyliczania zarobkow, ale podobnie do simulateOneDay zmienia wartosci nr dnia i nr tygodnia,
     * w celu prawidlowego liczenia czasu. Wyswietla aktualny stan dlugu na koniec tygodnia oraz wlicza pensje pracownikow i rachunki.
     * @see Salary
     * @see Bills
     */
    public void skipOneDay(){
        Salary salary = new Salary(rooms);
        Bills bills = new Bills(rooms);
        if(dayNumber==7){
            addDebt(salary.payDay());
            addDebt(bills.payments());
            dayNumber=1;
            weekNumber++;
            report.saveToFile(weekNumber);
            System.out.println("Debt after " + weekNumber + " week is: " + debt);
        }
        else{
            dayNumber++;
        }
    }

    /**
     * Metoda dodaje podana w parametrze wartosc do aktualnego dlugu.
     * @param payment wartosc dodawana do aktualnego długu
     */
    public void addDebt(long payment) {
        this.debt += payment;
    }

    /**
     * Metoda zwraca aktualny stan dlugu.
     * @return aktualny stan dlugu
     */
    public long getDebt(){
        return debt;
    }

    /**
     * Metoda do pobieranie informacji o aktualnym numerze tygodnia.
     * @return aktualny numer dnia
     */
    public int getDayNumber(){
        return dayNumber;
    }

    /**
     * Metoda do pobieranie informacji o aktualnym numerze dnia.
     * @return aktualny numer tygodnia
     */
    public int getWeekNumber(){
        return weekNumber;
    }

    /**
     * Metoda wywolujaca metody do zapisania przychodu z ostatniego, nie pelnego tygodnia do pliku
     * oraz pytajaca uzytkownika czy chce wyswietlic ten raport.
     */
    public void end(){
        report.lastSaveToFile(weekNumber);
        report.showReportFile();
    }
}
