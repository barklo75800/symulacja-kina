package com.simulation;

/**
 * Klasa Bills odpowiedzialna za wyliczanie sumy platnosci za rachunki.
 */
public class Bills {
    private final int rooms;

    /**
     * Konstruktor Bills.
     * @param rooms liczba sal w kinie
     */
    public Bills(int rooms){
        this.rooms = rooms;
    }

    /**
     * Metoda zliczajaca rachunki i zwracajaca ich sume.
     * @return suma platności za rachunki
     */
    public long payments(){
        long payment = 0;
        payment -= (long)1250 * rooms; //for electricity
        payment -= 525; //for water
        payment -= (long)8000 * rooms * 0.23; //for taxes
        return payment;
    }
}
