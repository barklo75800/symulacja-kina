package com.simulation;

import com.seance.Seance;
import com.shop.SnackType;
import com.tickets.NormalTicket;
import com.tickets.ReducedTicket;

/**
 * Klasa MovieIncome odpowiedzialna za przeprowadzenie pojedynczego seansu i zsumowanie przychodu.
 */
public class MovieIncome {
    private final int seats;

    /**
     * Konstruktor MovieIncome.
     * @param seats liczba siedzen w sali
     */
    public MovieIncome(int seats){
        this.seats = seats;
    }

    /**
     * Metoda tworząca obiekt seansu i wywolujaca metody tego obiektu w celu wyliczenia przychodu za jeden film.
     * @param equipmentPopularityModifier modyfikator popularnosci zalezny od rodzaju wyposazenia
     * @param normalTicket obiekt biletu normalego
     * @param reducedTicket obiekt biletu ulgowego
     * @param popcorn obiekt przekaski popcorn
     * @param nachos obiekt przekaski nachos
     * @param cola obiekt przekaski cola
     * @return zsumowany przychod z pojedynczego seansu
     * @see Seance
     */
    public long simulateOneMovie(double equipmentPopularityModifier, NormalTicket normalTicket, ReducedTicket reducedTicket, SnackType popcorn, SnackType nachos, SnackType cola){
        Seance seance = new Seance(seats);
        seance.setViewers(normalTicket.getPopularity(), equipmentPopularityModifier);
        long income = 0;
        income+=seance.ticketIncome(normalTicket.getPrice(), reducedTicket.getPrice());
        income+=seance.snackIncome(popcorn, nachos, cola);
        return income;
    }
}
