package com.simulation;

import com.staff.StaffDB;

/**
 * Klasa Salary odpowiedzialna za wyliczanie sumy wynagrodzen pracownikow.
 */
public class Salary {
    private final int rooms;

    /**
     * Konstruktor Salary.
     * @param rooms ilosc sal w kinie
     */
    public Salary(int rooms){
        this.rooms = rooms;
    }

    /**
     * Metoda zliczajaca i zwracajaca sume wynagrodzen pracownikow.
     * @return suma wynagrodzen pracownikow kina
     * @see StaffDB
     */
    public long payDay(){
        StaffDB staffDB = new StaffDB(rooms);
        long payment = 0;
        payment -= (long)staffDB.getNumber(staffDB.getCashiers()) * staffDB.getSalary(staffDB.getCashiers());
        payment -= (long)staffDB.getNumber(staffDB.getCleaners()) * staffDB.getSalary(staffDB.getCleaners());
        payment -= (long)staffDB.getNumber(staffDB.getProjectionists()) * staffDB.getSalary(staffDB.getProjectionists());
        payment -= (long)staffDB.getNumber(staffDB.getSecurity()) * staffDB.getSalary(staffDB.getSecurity());
        payment -= (long)staffDB.getNumber(staffDB.getTechnicians())* staffDB.getSalary(staffDB.getTechnicians());
        return payment;
    }
}
