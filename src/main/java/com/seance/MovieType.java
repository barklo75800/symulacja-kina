package com.seance;

/**
 * Klasa MovieType odpowiedzialna za przechowywanie informacji o podstawowej popularnosci filmu i procencie biletow ulgowych sprzedawanych na ten film.
 */
public class MovieType {
    private final double popularity;
    private final double percentageOfReducedTickets;

    /**
     * Konstruktor MovieType.
     * @param popularity podstawowa popularnosc filmu
     * @param percentageOfReducedTickets procent biletow ulgowych sprzedawanych na dany film
     */
    public MovieType(double popularity, double percentageOfReducedTickets) {
            this.popularity = popularity;
            this.percentageOfReducedTickets = percentageOfReducedTickets;
        }

    /**
     * Metoda zwraca informacje o podstawowej popularnosci danego gatunku filmu.
     * @return podstawowa popularnosc filmu
     */
    public double getPopularity() {
        return this.popularity;
    }

    /**
     * Metoda zwracajaca informacje o procencie biletow ulgowych sprzedanych na dany film.
     * @return procent biletow ulgowych sprzedawanych na dany film
     */
    public double getPercentageOfReducedTickets() {
        return this.percentageOfReducedTickets;
    }

}
