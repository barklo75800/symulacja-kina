package com.seance;

import  com.shop.SnackType;

/**
 * Klasa Seance dziedziczy po klasie MovieDB.
 * Odpowiedzialna za wylosowanie gatunku filmu i obliczenie widzow, ktorzy dany film obejrza.
 * @see MovieDB
 */
public class Seance extends MovieDB{
    private final int seats;
    private int spectators;
    private int normalSpectators;
    private int reducedSpectators;

    /**
     * Konstruktor Seance.
     * @param seats liczba siedzen w sali
     */
    public Seance(int seats){
        this.seats = seats;
        setMovieDB();
    }

    /**
     * Metoda losujaca gatunek filmu i liczaca liczbe widzow, ktora obejrzy ten film.
     * @param ticketPopularity modyfikator popularnosci w zaleznosci od cen biletow
     * @param equipmentPopularity modoyfikator popularnosci w zaleznosci od rodzaju wyposazenia
     */
    public void setViewers(double ticketPopularity, double equipmentPopularity) {
        int category = (int) Math.floor((Math.random() * 9 - 1) + 1);
        spectators = (int) (getCategoriesOfMovies().get(category).getPopularity() * seats * ticketPopularity * equipmentPopularity);
        reducedSpectators = (int) (spectators * getCategoriesOfMovies().get(category).getPercentageOfReducedTickets());
        normalSpectators = spectators - reducedSpectators;
    }

    /**
     * Metoda liczaca i zwracajaca przychod za sprzedaz biletow.
     * @param normalPrice cena biletu normalnego
     * @param reducedPrice cena biletu ulgowego
     * @return przychod z biletow
     */
    public long ticketIncome(double normalPrice, double reducedPrice){
        double income = (normalSpectators * normalPrice) + (reducedSpectators * reducedPrice);
        return (long)income;
    }

    /**
     * Metoda liczaca i zwracajaca przychod za sprzedaz przekasek.
     * @param popcorn obiekt popcorn
     * @param nachos obiekt nachos
     * @param cola obiekt cola
     * @return przychod za przekaski
     */
    public long snackIncome(SnackType popcorn, SnackType nachos, SnackType cola){
        double income = spectators * popcorn.getPopularity() * popcorn.getPrice() + spectators * nachos.getPopularity() * nachos.getPrice() + spectators * cola.getPopularity() * cola.getPrice();
        return (long)income;
    }
}
