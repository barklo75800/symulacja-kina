package com.seance;

import java.util.LinkedList;
import java.util.List;

/**
 * Klasa MovieDB tworzy i przechowuje obiekty roznych gatunkow filmow.
 * @see MovieType
 */
public class MovieDB {
    private final List<MovieType> categoriesOfMovies = new LinkedList<>();
    private final MovieType action = new MovieType(0.8, 0.2);
    private final MovieType adventure = new MovieType(0.7,0.2);
    private final MovieType comedy = new MovieType(0.7,0.4);
    private final MovieType thriller = new MovieType(0.4,0);
    private final MovieType romantic = new MovieType(0.3,0.1);
    private final MovieType documentary = new MovieType(0.1,0.2);
    private final MovieType cartoon = new MovieType(0.5,0.6);
    private final MovieType horror = new MovieType(0.5,0);
    private final MovieType scienceFiction = new MovieType(0.8,0.4);

    /**
     * Metoda zapisujaca kazdy utworzony gatunek filmu do listy categoriesOfMovies.
     */
    public void setMovieDB(){
        categoriesOfMovies.add(action);
        categoriesOfMovies.add(adventure);
        categoriesOfMovies.add(comedy);
        categoriesOfMovies.add(thriller);
        categoriesOfMovies.add(romantic);
        categoriesOfMovies.add(documentary);
        categoriesOfMovies.add(cartoon);
        categoriesOfMovies.add(horror);
        categoriesOfMovies.add(scienceFiction);
    }

    /**
     * Metoda zwracajaca liste zawierajaca gatunki filmow.
     * @return lista zawierajaca obiekty filmow
     */
    public List<MovieType> getCategoriesOfMovies() {
        return categoriesOfMovies;
    }
}
