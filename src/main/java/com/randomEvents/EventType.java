package com.randomEvents;

/**
 * Klasa EventType przechowuje informacje dotyczace pojedynczego wydarzenia losowego.
 */
public class EventType {
    private final int durationTime;
    private final double chanceOfOccurrence;
    private final String description;

    /**
     * Konstruktor EventType.
     * @param durationTime czas dzialania wydarzenia losowego
     * @param chanceOfOccurrence szansa wystąpienia wydarzenia losowego
     * @param description opis wydarzenia losowego
     */
    public EventType(int durationTime, double chanceOfOccurrence, String description){
        this.durationTime = durationTime;
        this.chanceOfOccurrence = chanceOfOccurrence;
        this.description = description;
    }

    /**
     * Metoda zwracajaca czas dzialania danego wydarzenia.
     * @return czas dzialania wydarzenia losowego
     */
    public int getDurationTime() {
        return durationTime;
    }

    /**
     * Metoda zwracajaca szanse wystapienia danego wydarzenia losowego.
     * @return szansa wystapienia wydarzenia losowego
     */
    public double getChanceOfOccurrence() {
        return chanceOfOccurrence;
    }

    /**
     * Metoda zwracajaca opis danego wydarzenia losowego.
     * @return opis wydarzenia losowego
     */
    public String getDescription() {
        return description;
    }
}
