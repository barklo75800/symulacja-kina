package com.randomEvents;

import com.simulation.TimeSimulation;

/**
 * Klasa EventOccurrence dziedziczy po klasie EventDB.
 * @see EventDB
 */
public class EventOccurrence extends EventDB {

    /**
     * Konstruktor EventOccurrence.
     */
    public EventOccurrence(){
        setEventDB();
    }

    /**
     * Metoda generujaca szanse wystapienia zdarzenia losowego.
     * @return wygenerowana szansa wystapienia
     */
    private double generateChance(){
        double chance;
        chance = Math.random();
        return chance;
    }

    /**
     * Metoda tworzy obiekt eventsOccurrence i wykorzystuje metode generateChance w celu okreslenia wystapienia wydarzen losowych.
     * Gdy nastepuje wydarzenie losowe to ta metoda wprowadza skutki tego wydarzenia do symulacji.
     * @param timeSimulation obiekt TimeSimulation
     */
    public void eventGenerator(TimeSimulation timeSimulation){
        EventOccurrence eventsOccurrence = new EventOccurrence();
        double chance = generateChance();
        for (int i=0; i<eventsOccurrence.getEventDB().size(); i++){
            if(chance<eventsOccurrence.getEventDB().get(i).getChanceOfOccurrence()){
                switch(i){
                    case 0:
                        System.out.println(eventsOccurrence.getEventDB().get(0).getDescription());
                        System.exit(1);
                    case 1:
                        System.out.println(eventsOccurrence.getEventDB().get(1).getDescription());
                        timeSimulation.addDebt(50000);
                    case 2:
                        System.out.println(eventsOccurrence.getEventDB().get(2).getDescription());
                        for(int j=0;j<eventsOccurrence.getEventDB().get(2).getDurationTime();j++) {
                            timeSimulation.skipOneDay();
                        }
                    case 3:
                        System.out.println(eventsOccurrence.getEventDB().get(3).getDescription());
                        timeSimulation.addDebt(-100000);
                        for(int j=0;j<eventsOccurrence.getEventDB().get(3).getDurationTime();j++) {
                            timeSimulation.skipOneDay();
                        }
                    case 4:
                        System.out.println(eventsOccurrence.getEventDB().get(4).getDescription());
                        for(int j=0;j<eventsOccurrence.getEventDB().get(4).getDurationTime();j++) {
                            timeSimulation.skipOneDay();
                        }
                    default:
                }
            }
            else
                timeSimulation.simulateOneDay();
        }
    }
}
