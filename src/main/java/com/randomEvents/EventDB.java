package com.randomEvents;

import java.util.LinkedList;
import java.util.List;

/**
 * Klasa EventDB przechowuje i tworzy obieky  bedace roznymi wydarzeniami losowymi.
 * @see EventType
 */
public class EventDB {
    private final List<EventType> randomEvents = new LinkedList<>();
    private final EventType deathStar = new EventType(0,0.000001,"Death Star destroyed Earth so cinema disappeared. Unexpected end of simulation.");
    private final EventType aliensVisit = new EventType(0,0.0001,"Aliens arrived to watch movies in cinema. Cinema made additional income of 50 000.");
    private final EventType globalPandemic = new EventType(28,0.0003,"Global pandemic forced cinema to close for 4 weeks which means no income.");
    private final EventType JSOSFailure = new EventType(3,0.003,"Failure of JSOS in all rooms. Cinema had to be closed for 3 day. Repair cost  100 000.");
    private final EventType electricityFailure = new EventType(1,0.005,"For entire day cinema didn't have electricity and couldn't work properly.");

    /**
     * Metoda wpisujaca do listy randomEvents wszystkie obiekty wydarzen losowych.
     */
    public void setEventDB(){
        randomEvents.add(deathStar);
        randomEvents.add(aliensVisit);
        randomEvents.add(globalPandemic);
        randomEvents.add(JSOSFailure);
        randomEvents.add(electricityFailure);
    }

    /**
     * Metoda zwracajaca liste wydarzen losowych.
     * @return lista z wydarzeniami losowymi
     */
    public List<EventType> getEventDB(){
        return randomEvents;
    }
}
