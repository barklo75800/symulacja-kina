package com;

import com.simulation.TimeSimulation;

/**
 * Glowna klasa Main, od ktorej zaczyna sie symulacja.
 * @author Zofia Zub, Maciej Cieslak, Bartosz Kloc
 */
public class Main {

    /**
     * Metoda main tworzaca obiekty:
     * input, w celu pobrania od uzytkownika parametrow poczatkowych symulacji,
     * timeSimulation, ktora prowadzi przebieg symulacji.
     * Petla while sprawdza czy symulacja ma dalej trwac.
     * Na koniec symulacji wyswietla po jakim czasie sie ona zakonczyla.
     * @param args parametr pobierany z konsoli podczas wywolywania programu
     * @see Input
     * @see TimeSimulation
     */
    public static void main(String[] args) {
        Input input = new Input();
        input.buildCinema();
        input.setPrices();

        TimeSimulation timeSimulation = new TimeSimulation(input.getRooms(), input.getSeats(), -input.getTotalCost(), input.getEquipmentPopularityModifier() ,input.ticketPriceProvider.getNormalTicket(), input.ticketPriceProvider.getReducedTicket(), input.snackProvider.getPopcorn(), input.snackProvider.getNachos(), input.snackProvider.getCola(), input.enablingRandomEvents());

        while (timeSimulation.getDebt()<0) {
            timeSimulation.simulate();
        }

        if(timeSimulation.getDayNumber()==7) {
            System.out.println("Simulation ended after " + (timeSimulation.getWeekNumber()+1) + " weeks");
        }
        else {
            System.out.println("Simulation ended after " + timeSimulation.getWeekNumber() + " weeks and " + timeSimulation.getDayNumber() + " days");
        }

        timeSimulation.end();
    }
}
