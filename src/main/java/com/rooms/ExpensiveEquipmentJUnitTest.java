package com.rooms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExpensiveEquipmentJUnitTest {

    @Test
    void totalPrice() {
        long total;
        total = (long)1750000 * 7 + (long)1000 * 200 * 7;
        assertEquals(13650000,total);
        total = (long)1750000 * 10 + (long)1000 * 150 * 10;
        assertEquals(19000000,total);
    }
}