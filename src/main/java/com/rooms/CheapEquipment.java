package com.rooms;

/**
 * Klasa CheapEquipment implementujaca interfejs Equipment.
 * @see Equipment
 */
public class CheapEquipment implements Equipment {
    private final int costOfSeats = 500;
    private final double popularityModifier = 0.8;

    /**
     * Metoda liczaca i zwracajaca calkowity koszt otwarcia kina.
     * @param seats liczba siedzen w sali
     * @param rooms liczba sal w kinie
     * @return calkowity koszt otwarcia kina
     */
    @Override
    public long totalPrice(int seats, int rooms) {
        long total;
        total = (long)1350000 * rooms + (long)costOfSeats * seats;
        return total;
    }

    /**
     * Metoda zwracajaca modyfikator popularnosci zalezny od rodzaju wyposazenia.
     * @return modyfikator popularnosci zalezny od rodzaju wyposazenia
     */
    @Override
    public double getPopularityModifier(){
        return popularityModifier;
    }
}