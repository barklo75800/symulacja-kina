package com.rooms;

/**
 * Interfejs Equipment implenetowany przez rodzaje wyposazenia.
 */
public interface Equipment {
    /**
     * Metoda zwracajaca calkowity koszt otwarcia kina.
     * @param seats liczba siedzen w sali
     * @param rooms liczba sal w kinie
     * @return calkowity koszt otwarcia kina
     */
    long totalPrice(int seats, int rooms);

    /**
     * Metoda zwracajaca modyfikator popularnosci zalezny od rodzaju wyposazenia.
     * @return modyfikator popularnosci zalezny od rodzaju wyposazenia
     */
    double getPopularityModifier();
}
