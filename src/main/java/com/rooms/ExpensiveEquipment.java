package com.rooms;

/**
 * Klasa ExpensiveEquipment implementujaca interfejs Equipment.
 * @see Equipment
 */
public class ExpensiveEquipment implements Equipment {
    private final int costOfSeats = 1000;
    private final double popularityModifier = 1.2;

    /**
     * Metoda liczaca i zwracajaca calkowity koszt otwarcia kina.
     * @param seats liczba siedzen w sali
     * @param rooms liczba sal w kinie
     * @return calkowity koszt otwarcia kina
     */
    @Override
    public long totalPrice(int seats, int rooms) {
        long total;
        total = (long)1750000 * rooms + (long)costOfSeats * seats * rooms;
        return total;
    }

    /**
     * Metoda zwracajaca modyfikator popularnosci zalezny od rodzaju wyposazenia.
     * @return modyfikator popularnosci zalezny od rodzaju wyposazenia
     */
    @Override
    public double getPopularityModifier(){
        return popularityModifier;
    }
}