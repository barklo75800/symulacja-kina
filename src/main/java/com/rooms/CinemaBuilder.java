package com.rooms;

import java.util.Scanner;

/**
 * Klasa CinemaBuilder tworzy i przechowuje skaner uzywany przez metode build.
 * Przechowuje prywatne zmienne totalCost, seats oraz koncowa zmienna numberOfRooms.
 */
public class CinemaBuilder {
    private final int seats;
    private final int numberOfRooms;
    private long totalCost;
    private double equipmentPopularityModifier;

    /**
     * Konstruktor CinemaBuilder.
     */
    public CinemaBuilder(int numberOfRooms, int seats) {
        this.numberOfRooms = numberOfRooms;
        this.seats = seats;
    }

    /**
     * Metoda pobierajaca od uzytkownika rodzaj wybranego wyposazenia w kinie.
     */
    public void build(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose the type of equipment [CHEAP, NORMAL, EXPENSIVE] ");
        boolean correct = false;
        while(!correct){
            String type = scanner.nextLine();
            switch (type.toUpperCase()){
                case "CHEAP" -> {
                    Equipment equipment = new CheapEquipment();
                    this.totalCost=equipment.totalPrice(seats, numberOfRooms);
                    this.equipmentPopularityModifier=equipment.getPopularityModifier();
                    correct = true;
                }
                case "NORMAL" -> {
                    Equipment equipment = new MediumEquipment();
                    this.totalCost=equipment.totalPrice(seats, numberOfRooms);
                    this.equipmentPopularityModifier=equipment.getPopularityModifier();
                    correct = true;
                }
                case "EXPENSIVE" -> {
                    Equipment equipment = new ExpensiveEquipment();
                    this.totalCost=equipment.totalPrice(seats, numberOfRooms);
                    this.equipmentPopularityModifier=equipment.getPopularityModifier();
                    correct = true;
                }
                default -> System.out.println("Read error! Try again:");
            }

        }
    }

    /**
     * Metoda zwracajaca calkowity koszt otwarcia kina.
     */
    public long getTotalCost() {
        return totalCost;
    }

    /**
     * Metoda zwracajaca modyfikator popularnosci w zaleznosci od rodzaju wybranego wyposazenia.
     */
    public double getEquipmentPopularityModifier(){
        return equipmentPopularityModifier;
    }
}
