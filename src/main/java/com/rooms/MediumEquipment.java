package com.rooms;

/**
 * Klasa MediumEquipment implementujaca interfejs Equipment.
 * @see Equipment
 */
public class MediumEquipment implements Equipment {
    private final int costOfSeats = 750;
    private final double popularityModifier = 1;

    /**
     * Metoda liczaca i zwracajaca calkowity koszt otwarcia kina.
     * @param seats liczba siedzen w sali
     * @param rooms liczba sal w kinie
     * @return calkowity koszt otwarcia kina
     */
    @Override
    public long totalPrice(int seats, int rooms) {
        long total;
        total = (long)1550000 * rooms + (long)costOfSeats * seats;
        return total;
    }

    /**
     * Metoda zwracajaca modyfikator popularnosci zalezny od rodzaju wyposazenia.
     * @return modyfikator popularnosci zalezny od rodzaju wyposazenia
     */
    @Override
    public double getPopularityModifier(){
        return popularityModifier;
    }
}