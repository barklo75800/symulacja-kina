package com;

import com.rooms.*;
import com.shop.SnackProvider;
import com.tickets.TicketPriceProvider;

import java.util.Scanner;

/**
 * Klasa Input odpowiedzialna za odczytanie od uzytkownika parametrow symulacji
 * oraz stworzenie kina poprzez wywolywanie innych klas i metod.
 * @see TicketPriceProvider
 * @see SnackProvider
 */
public class Input {
    TicketPriceProvider ticketPriceProvider = new TicketPriceProvider();
    SnackProvider snackProvider = new SnackProvider();
    private int rooms, seats;
    private long totalCost;
    private double equipmentPopularityModifier;

    /**
     * Metoda do wywolywania metod w obiektach snackProvider i ticketPriceProvider,
     * ktore po wywolaniu pobieraja od uzytkownika informacje o cenach danych przedmiotow.
     */
    public void setPrices() {
        snackProvider.setSnackPrices();
        ticketPriceProvider.settingPrices();
    }

    /**
     * Metoda wykorzystujaca skaner do odczytania ilosci sal i miejsc w salach od uzytkownika.
     * Nastepnie tworzy obiekt cinemaBuilder, w ktorym wywoluje metode build.
     */
    public void buildCinema() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of rooms [2-20]: ");
        do{
            while(!scanner.hasNextInt()){
                String error = scanner.next();
                System.out.println(error + " is not a number! Try again:");
            }
            rooms = scanner.nextInt();
            if(rooms<2 || rooms>20){
                System.out.println("Not allowed number! Try again:");
            }
        }while(rooms<2 || rooms>20);

        System.out.println("Enter number of seats in each room [50,100,150,200,250]: ");
        do{
            while(!scanner.hasNextInt()){
                String error = scanner.next();
                System.out.println(error + " is not a number! Try again:");
            }
            seats = scanner.nextInt();
            if(seats!=50 && seats!=100 && seats!=150 && seats!=200 && seats !=250){
                System.out.println("Not allowed number! Try again:");
            }
        }while(seats!=50 && seats!=100 && seats!=150 && seats!=200 && seats !=250);

        CinemaBuilder cinemaBuilder = new CinemaBuilder(rooms,seats);
        cinemaBuilder.build();
        totalCost = cinemaBuilder.getTotalCost();
        equipmentPopularityModifier = cinemaBuilder.getEquipmentPopularityModifier();
    }

    /**
     * Metoda pytajaca uzytkownika o wlaczenie wydarzen losowych, ktore moga wystapic w symulacji.
     * @return boolean, ktory odpowiada czy wydarzenia zostaly wlaczone
     */
    public boolean enablingRandomEvents(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to enable occurrence of random events? [YES/NO]");
        while(true) {
            String type = scanner.nextLine();
            switch (type.toUpperCase()) {
                case "YES" -> {
                    return true;
                }
                case "NO" -> {
                    return false;
                }
                default -> System.out.println("Read error! Try again:");
            }
        }
    }

    /**
     * Metoda do pobrania ilosci sal kinowych ustalonej przez uzytkownika.
     * @return ilosc sal w kinie
     */
    public int getRooms() {
        return rooms;
    }

    /**
     * Metoda do pobrania ilosci siedzien w sali ustalonej przez uzytkownika.
     * @return ilosc siedzien w pojedynczej sali
     */
    public int getSeats() {
        return seats;
    }

    /**
     * Metoda do pobrania calkowitego kosztu otwarcia kina.
     * @return calkowity koszt otwarcia kina
     */
    public long getTotalCost() {
        return totalCost;
    }

    /**
     * Metoda do pobrania modyfikatora popularnosci w zalezny od rodzaju wyposazenia.
     * @return modyfikator popularnosci zalezny od rodzaju wyposazenia
     */
    public double getEquipmentPopularityModifier(){
        return equipmentPopularityModifier;
    }
}

