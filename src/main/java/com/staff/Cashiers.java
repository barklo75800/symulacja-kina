package com.staff;

/**
 * Klasa Cashiers implementuje interfejs Staff.
 * Przechowuje wynagrodzenie i ilosc osob na stanowisku kasjera.
 * @see Staff
 */
public class Cashiers implements Staff{
    private final int salary;
    private int number;

    /**
     * Konstruktor klasy Cashiers.
     * @param salary wynagrodzenie kasjera
     */
    public Cashiers (int salary){
        this.salary = salary;
    }

    /**
     * Metoda do ustalenia liczby kasjerow.
     * @param rooms ilosc sal w kinie, od ktorej zalezna jest ilosc pracownikow na danych stanowiskach
     */

    public void setNumber(int rooms) {
        this.number = 2;
    }

    /**
     * Metoda do pobrania ilosci kajserow w kinie.
     * @return liczba kasjerow
     */
    @Override
    public int getNumber() {
        return this.number;
    }

    /**
     * Metoda do pobrania wynagrodzenia kajsera.
     * @return wynagrodzenie kasjera
     */
    @Override
    public int getSalary() {
        return this.salary;
    }
}
