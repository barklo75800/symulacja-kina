package com.staff;

/**
 * Klasa Security implementuje interfejs Staff.
 * Przechowuje wynagrodzenie i ilosc osob na stanowisku ochroniarza.
 * @see Staff
 */
public class Security implements Staff{
    private final int salary;
    private int number;

    /**
     * Konstuktor klasy Security.
     * @param salary wynagrodzenie ochroniarza
     */
    public Security (int salary){
        this.salary = salary;
    }

    /**
     * Metoda do ustalenie liczby ochroniarzy.
     * @param rooms ilosc sal w kinie, od ktorej zalezna jest ilosc pracownikow na danych stanowiskach
     */
    @Override
    public void setNumber(int rooms) {
        this.number = 2;
    }

    /**
     * Metoda do pobrania liczby ochroniarzy.
     * @return liczba ochroniarzy
     */
    @Override
    public int getNumber() {
        return this.number;
    }

    /**
     * Metoda do pobrania wynagrodzenia ochroniarza.
     * @return wynagrodzenie ochroniarza
     */
    @Override
    public int getSalary() {
        return this.salary;
    }
}
