package com.staff;

/**
 * Klasa Cleaners implementuje interfejs Staff.
 * Przechowuje wynagrodzenie i ilosc osob na stanowisku sprzatacza.
 * @see Staff
 */
public class Cleaners implements Staff{
    private final int salary;
    private  int number;

    /**
     * Konstruktor klasy Cleaners.
     * @param salary wynagrodzenie sprzatacza
     */
    public Cleaners(int salary) {
        this.salary = salary;
    }

    /**
     * Metoda do ustalenia liczby sprzataczy na podstawie ilosci sal.
     * @param rooms ilosc sal w kinie, od ktorej zalezna jest ilosc pracownikow na danych stanowiskach
     */
    @Override
    public void setNumber(int rooms) {
        this.number = rooms * 4;
    }

    /**
     * Metoda do pobrania liczby sprzataczy w kinie.
     * @return liczba sprzataczy
     */
    @Override
    public int getNumber() {
        return this.number;
    }

    /**
     * Metoda do pobrania wynagrodzenia sprzatacza.
     * @return wynagrodzenie sprzatacza
     */
    @Override
    public int getSalary() {
        return this.salary;
    }
}