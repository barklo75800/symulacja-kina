package com.staff;

/**
 * Klasa Projectionists impelementuje interfejs Staff.
 * Przechowuje wynagrodzenie i ilosc osob na stanowisku osoby obslugujacej rzutnik.
 * @see Staff
 */
public class Projectionists implements Staff{
    private final int salary;
    private int number;

    /**
     * Konstuktor klasy Projectionists.
     * @param salary wynagrodzenie osoby obslugujacej rzutnik
     */
    public Projectionists (int salary){
        this.salary = salary;
    }

    /**
     *
     * Metoda do ustalenia liczby osob obslugujacych rzutniki w kinie na podstawie liczby sal.
     * @param rooms ilosc sal w kinie, od ktorej zalezna jest ilosc pracownikow na danych stanowiskach
     */
    @Override
    public void setNumber(int rooms) {
        this.number = 2 * rooms;
    }

    /**
     * Metoda do pobrania liczby osob obslugujących rzutniki w kinie.
     * @return liczba osob obslugujacych rzutniki w kinie
     */
    @Override
    public int getNumber() {
        return this.number;
    }

    /**
     * Metoda do pobrania wynagrodzenia osoby obslugujacej rzutnik.
     * @return wynagrodzenie osoby obslugujacej rzutnik
     */
    @Override
    public int getSalary() {
        return this.salary;
    }
}
