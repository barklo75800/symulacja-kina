package com.staff;

/**
 * Klasa StaffDB tworzy i przechowuje obiekty klas personelu.
 * @see Staff
 * @see Projectionists
 * @see Security
 * @see Cleaners
 * @see Technicians
 * @see Cashiers
 */
public class StaffDB {
    private final Staff projectionists = new Projectionists(1250);
    private final Staff security = new Security(750);
    private final Staff cleaners = new Cleaners(850);
    private final Staff technicians = new Technicians(2700);
    private final Staff cashiers = new Cashiers(950);

    /**
     * Konstruktor StaffDB.
     * @param rooms ilosc sal w kinie, od ktorej zalezna jest ilosc pracownikow na danych stanowiskach
     */
    public StaffDB(int rooms){
       projectionists.setNumber(rooms);
       security.setNumber(rooms);
       cleaners.setNumber(rooms);
       technicians.setNumber(rooms);
       cashiers.setNumber(rooms);
    }

    /**
     * Metoda do pobrania ilosci osob na stanowisku podanego obiektu personelu wykorzystujac interfejs Staff.
     * @param staff nazwa klasy personelu, dla ktorego ma byc uzyta metoda, uzywajaca interfejsu Staff
     * @return ilosc osob na podanym stanowisku
     * @see Staff
     */
    public int getNumber(Staff staff) {
        return staff.getNumber();
    }

    /**
     * Metoda do pobrania wynagrodzenia podanego obiektu personelu wykorzystujac interfejs Staff.
     * @param staff nazwa klasy personelu, dla ktorego ma byc uzyta metoda, uzywajaca interfejsu Staff
     * @return wynagrodzenie osoby na podanym stanowisku
     * @see Staff
     */
    public int getSalary(Staff staff) {
        return staff.getSalary();
    }

    /**
     * Metoda do pobrania obiektu obslugi rzutnika, wykorzystujaca interfejs Staff.
     * @return obiekt obslugi rzutnika
     * @see Staff
     */
    public Staff getProjectionists() {
        return projectionists;
    }

    /**
     * Metoda do pobrania obiektu ochroniarzy, wykorzystująca interfejs Staff.
     * @return obiekt ochroniarzy
     * @see Staff
     */
    public Staff getSecurity() {
        return security;
    }

    /**
     * Metoda do pobrania obiektu sprzataczy, wykorzystujaca interfejs Staff.
     * @return obiekt sprzataczy
     * @see Staff
     */
    public Staff getCleaners() {
        return cleaners;
    }

    /**
     * Metoda do pobrania obiektu technikow, wykorzystujaca interfejs Staff.
     * @return obiekt technikow
     * @see Staff
     */
    public Staff getTechnicians() {
        return technicians;
    }

    /**
     * Metoda do pobrania obiektu kasjerow, wykorzystujaca interfejs Staff.
     * @return obiekt kasjerow
     * @see Staff
     */
    public Staff getCashiers() {
        return cashiers;
    }
}
