package com.staff;

/**
 * Interfejs Staff implementowany przez klasy personelu.
 */
public interface Staff {
    /**
     * Metoda do ustalania liczby osob na danym stanowisku na podstawie liczby sal.
     * @param rooms ilosc sal w kinie, od ktorej zalezna jest ilosc pracownikow na danych stanowiskach
     */
    void setNumber(int rooms);
    /**
     * Metoda do pobrania liczby osob na danym stanowisku.
     * @return ilosc osob na stanowisku
     */
    int getNumber();
    /**
     * Metoda do pobrania wynagrodzenia osoby na danym stanowisku.
     * @return wynagrodzenie osoby na stanowisku
     */
    int getSalary();
}
