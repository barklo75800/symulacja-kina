package com.staff;

/**
 * Klasa Technicians implementuje interfejs Staff.
 * Przechowuje wynagrodzenie i ilosc osob na stanowisku technika.
 * @see Staff
 */
public class Technicians implements Staff{
    private final int salary;
    private int number;

    /**
     * Konstruktor StaffDB.
     * @param salary wynagrodzenie technika
     */
    public Technicians (int salary){
        this.salary = salary;
    }

    /**
     * Metoda do ustalenia liczby technikow.
     * @param rooms ilosc sal w kinie, od ktorej zalezne sa ilosci pracownikow na pewnych stanowiskach
     */
    @Override
    public void setNumber(int rooms) {
        this.number = 2;
    }

    /**
     * Metoda do pobrania liczby technikow.
     * @return liczba techników
     */
    @Override
    public int getNumber() {
        return this.number;
    }

    /**
     * Metoda do pobrania wynagrodzenia technika.
     * @return wynagrodzenie technika
     */
    @Override
    public int getSalary() {
        return this.salary;
    }
}
