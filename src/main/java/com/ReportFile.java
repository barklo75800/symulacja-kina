package com;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Klasa ReportFile odpowiedzialna za stworzenie i zapisywanie tygodniowych przychodow do pliku report.txt.
 */
public class ReportFile {
    File myFile;
    private long tempIncome = 0;

    /**
     * Metoda tworzy plik report.txt, do ktorego zapisywane beda sumy przychodow z kazdego tygodnia.
     */
    public void createReport(){
        myFile = new File("report.txt");
        if(myFile.exists()){
            myFile.delete();
        }
        try {
            myFile.createNewFile();
            FileWriter myWriter = new FileWriter(myFile, true);
            myWriter.write("This file contains income from every full week that was simulated.\r\n");
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda zapisuje tempIncome do pliku jako sume przychodow z tygodnia.
     * @param weekNumber aktualny nr tygodnia
     */
    public void saveToFile(int weekNumber){
        try {
            FileWriter myWriter = new FileWriter(myFile,true);
            myWriter.write("Income from week " + weekNumber + " is equal " + tempIncome + "\r\n");
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        resetTempIncome();
    }

    /**
     * Metoda wywolywana w celu zapisania tempIncome z trwajacego tygodnia podczas, ktorego symulacja konczy sie.
     * @param weekNumber aktualny nr tygodnia
     */
    public void lastSaveToFile(int weekNumber){
        try {
            FileWriter myWriter = new FileWriter(myFile,true);
            myWriter.write("Income from last, probably not full, week " + weekNumber + " is equal " + tempIncome + "\r\n");
            myWriter.write("Simulation ended during last week!");
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda dodaje wartosc podana jako parametr do tempIncome.
     * @param value wartosc dodawana do tempIncome
     */
    public void addTempIncome(long value){
        tempIncome += value;
    }

    /**
     * Metoda zeruje tempIncome po zapisaniu go do pliku.
     */
    private void resetTempIncome(){
        tempIncome = 0;
    }

    /**
     * Metoda otwierajaca plik raportu.
     */
    public void showReportFile(){
        System.out.println("Opening report file...");
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.open(myFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
