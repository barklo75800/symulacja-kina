package com.shop;

/**
 * Klasa SnackType odpowiedzialna za przechowywanie ceny i modyfikatora popularnosci przekaski w zaleznosci od ceny.
 */
public class SnackType {
    private double price;
    private double popularity;

    /**
     * Metoda do ustalenia ceny i stopnia popularnosci przekaski.
     * @param price cena przekaski
     * @param snackPopularity popularnosc przekaski przed modyfikacja
     */
    public void setPriceAndPopularity(double price, double snackPopularity) {
        this.price = price;
        this.popularity = snackPopularity * Math.round(((-price/25)+1.4)*100.0)/100.0;
    }

    /**
     * Metoda do pobrania informacji o cenie przekaski.
     * @return cena przekaski
     */
    public double getPrice() {
        return this.price;
    }

    /**
     * Metoda do pobrania informacji o stopniu popularnosci przekaski.
     * @return modyfikator popularnosci w zaleznosci od rodzaju i ceny przekaski
     */
    public double getPopularity() {
        return this.popularity;
    }
}
