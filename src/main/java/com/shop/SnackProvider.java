package com.shop;

import java.util.Scanner;

/**
 * Klasa SnackProvider tworzy i przechowuje obiekty przekasek.
 * @see SnackType
 */
public class SnackProvider {
    private final SnackType popcorn = new SnackType();
    private final SnackType cola = new SnackType();
    private final SnackType nachos = new SnackType();

    /**
     * Metoda pobierajaca od uzytkownika ceny wszystkich przekasek.
     * @param snackType obiekt przekaski, o ktorej cene pytany jest uzytkownik przy uruchomieniu programu
     * @param name nazwa przekaski uzywana przy wyswietlaniu w konsoli
     * @param snackPopularity podstawowa popularnosc przekaski przed modyfikacja
     */
    public void setPrice(SnackType snackType, String name, double snackPopularity){
        int value;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter price of "+name+": [5-15]");
        do{
            while(!scanner.hasNextInt()){
                String error = scanner.next();
                System.out.println(error + " is not a number! Try again:");
            }
            value = scanner.nextInt();
            if(value<5 || value>15){
                System.out.println("Not allowed number! Try again:");
            }
        }while(value<5 || value>15);
        snackType.setPriceAndPopularity(value, snackPopularity);
    }

    /**
     * Metoda wywolujaca metode do ustalenia cen przekasek dla kazdej utworzonej przekaski.
     */
    public void setSnackPrices(){
        setPrice(popcorn, "popcorn", 0.3);
        setPrice(nachos, "nachos", 0.1);
        setPrice(cola, "cola", 0.5);
    }

    /**
     * Metoda do pobrania obiektu Popcorn.
     * @return obiekt popcorn
     */
    public SnackType getPopcorn() {
        return popcorn;
    }

    /**
     * Metoda do pobrania obiektu Nachos.
     * @return obiekt nachos
     */
    public SnackType getNachos() {
        return nachos;
    }

    /**
     * Metoda do pobrania obiektu Cola.
     * @return obiekt cola
     */
    public SnackType getCola() {
        return cola;
    }
}
